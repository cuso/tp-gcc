from django.urls import path
from .views import CrearUsuario
from django.contrib.auth.decorators import login_required

urlpatterns = [    
    path('crear_usuario/',login_required(CrearUsuario.as_view()),name=" crear_usuario "),
]