import unittest
from django.test import TestCase
from django.test.client import Client
from django.contrib.auth.models import User

"""
Tests de comprobacion para el login
"""
class LoginTest(TestCase):
    def test_login_invalido(self):
        """
        valida que un usuario no registrado no pueda entrar al sistema
        """
        self.client = Client()
        login = self.client.login(username='noRegistrado', password='1234567!')
        self.assertEqual(login, False, 'Prueba unitaria de logueo invalido fallo')