from django.urls import path
from .views import crearAutor,listarAutor
from django.contrib.auth.decorators import login_required

urlpatterns = [
    path('crear_autor/',login_required(crearAutor), name = 'crear_autor'),
    path('listar_autor/',login_required(listarAutor), name = 'listar_autor')
]